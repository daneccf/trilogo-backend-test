﻿using System.ComponentModel.DataAnnotations;

namespace Tickets.API.Models
{

    public class IdentityViewModel {

        public class RegisterUserViewModel {
            [Required(ErrorMessage = "O campo {0} é obrigatório")]
            [EmailAddress(ErrorMessage = "O campo {0} está em formato inválido")]

            public string Email { get; set; }
            [Required(ErrorMessage = "O campos {0} é obrigatório")]
            public string Password { get; set; }
            [Compare("Password", ErrorMessage = "As senhas não conferem.")]
            public string ConfirmPassword { get; set; }

        }

        public class LoginUserViewModel {
            [Required(ErrorMessage = "O campo {0} é obrigatório")]
            [EmailAddress(ErrorMessage = "O campo {0} está em formato inválido")]

            public string Email { get; set; }
            [Required(ErrorMessage = "O campo {0} é obrigatório")]
            public string Password { get; set; }
        }
    }
}
