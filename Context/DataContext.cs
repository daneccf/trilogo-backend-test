using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tickets.API.Models {
    public class DataContext : IdentityDbContext {
        public DataContext(DbContextOptions<DataContext> option)
            : base(option) { }

        public DbSet<Ticket> Tickets { get; set; }
    }
}