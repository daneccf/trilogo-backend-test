# Desafio Backend

# Framework and SDK
- Compatible for `Visual Studio 2019` and `Visual Studio Code`
- Base on `.NET Core 3.1`
- `Entity Framework` and `Dapper` for ORM
- `JWT` and `ASP.NET Identity` for authentication
- SQL Server

# Steps to run:
- Create your database manually and update database connection string in `appsettings.json`
- To get the access token, call `POST: /api/signup` to register a new account using you email and password 

``` json
    POST /api/signup
    {
      "email":"teste@email.com",
      "password":"Teste@123",
      "confirmPassword":"Teste@123"
    }
```
- Call `POST: /api/signin` and enter your registered email and password
``` json
    POST /api/signin
    {
      "email":"teste@gmail.com",
      "password":"Teste@123"
    }
```
- Now that you have the authorization token and API access, you can use the following endpoints to manage tickets
  
### POST `/api/tickets/`
This endpoint receives a JSON object to create a new ticket

```json
{
   "Description":"Lâmpada queimada",
   "AuthorName":"Washington",
   "Date":"26/11/2015",
}
```

### PUT `/api/tickets/{id}`
Endpoint that changes a ticket for a given _id_

_FromForm_
```json
{
   "Description":"Lâmpada com mal contato",
   "AuthorName":"null", (optional)
   "Date":"null", (optional)
}
```

### DELETE `/api/tickets/{id}`
Deletes a specific ticket with _id_ passed as a parameter

### GET `/api/tickets`
Lists all registered tickets:

```json
[
  {
   "Id":1,
   "Description":"Lâmpada queimada",
   "AuthorName":"Washington",
   "Date":"26/11/2019"
  },
  {
   "Id":2,
   "Description":"Pintar parede",
   "AuthorName":"Pedro",
   "Date":"12/12/2019"
  },
  {
   "Id":3,
   "Description":"Monitor com defeito",
   "AuthorName":"João",
   "Date":"07/01/2020"
  },
]
```